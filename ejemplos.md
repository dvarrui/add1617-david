
#Ejemplos de Markdown

##Textos

Primer archivo Markdown.

* Hola
* Hello
* Halo

1. Hola
1. Cómo
1. Estás
1. bien

##Más textos

Ruta de los archivos `/etc/resolv.conf`.

> Esto es una anotación

Ahora [ir a google](https://www.google.es).

![yoda](./images/yoda.jpeg)
